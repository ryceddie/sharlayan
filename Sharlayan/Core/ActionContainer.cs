// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActionContainer.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   ActionContainer.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Core {
    using System.Collections.Generic;

    using Sharlayan.Core.Enums;
    using Sharlayan.Core.Interfaces;

    public class ActionContainer : IActionContainer {
        public List<HotBarRecastItem> Actions { get; } = new List<HotBarRecastItem>();

        public HotBarRecast.Container Type { get; set; }
    }
}