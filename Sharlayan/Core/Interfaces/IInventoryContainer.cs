﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInventoryContainer.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   IInventoryContainer.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Core.Interfaces {
    using System.Collections.Generic;

    using Sharlayan.Core.Enums;

    public interface IInventoryContainer {
        uint Amount { get; set; }

        List<InventoryItem> Items { get; }

        Inventory.Container Type { get; set; }

        byte TypeID { get; set; }
    }
}