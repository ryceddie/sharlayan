﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InventoryContainer.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   InventoryContainer.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Core {
    using System.Collections.Generic;

    using Sharlayan.Core.Enums;
    using Sharlayan.Core.Interfaces;

    public class InventoryContainer : IInventoryContainer {
        public uint Amount { get; set; }

        public List<InventoryItem> Items { get; } = new List<InventoryItem>();

        public Inventory.Container Type { get; set; }

        public byte TypeID { get; set; }
    }
}