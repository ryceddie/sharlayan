﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Reader.Actions.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   Reader.Actions.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan {
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Sharlayan.Core;
    using Sharlayan.Core.Enums;
    using Sharlayan.Models.ReadResults;

    using BitConverter = Sharlayan.Helpers.BitConverter;

    public static partial class Reader {
        public static bool CanGetActions() {
            var canRead = Scanner.Instance.Locations.ContainsKey(Signatures.HotBarKey) && Scanner.Instance.Locations.ContainsKey(Signatures.RecastKey);
            if (canRead) {
                // OTHER STUFF
            }

            return canRead;
        }

        public static ActionResult GetActions() {
            var result = new ActionResult();

            if (!CanGetActions() || !MemoryHandler.Instance.IsAttached) {
                return result;
            }

            try {
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_1));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_2));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_3));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_4));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_5));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_6));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_7));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_8));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_9));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.HOTBAR_10));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_1));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_2));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_3));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_4));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_5));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_6));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_7));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_HOTBAR_8));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.PETBAR));
                result.Containers.Add(GetHotBarRecast(HotBarRecast.Container.CROSS_PETBAR));
            }
            catch (Exception ex) {
                MemoryHandler.Instance.RaiseException(Logger, ex, true);
            }

            return result;
        }

        private static ActionContainer GetHotBarRecast(HotBarRecast.Container type) {
            var HotBarMap = Scanner.Instance.Locations[Signatures.HotBarKey];
            var RecastMap = Scanner.Instance.Locations[Signatures.RecastKey];

            var hotbarContainerSize = MemoryHandler.Instance.Structures.HotBarItem.ContainerSize;
            var hotbarContainerAddress = IntPtr.Add(HotBarMap, (int) type * hotbarContainerSize);

            var recastContainerSize = MemoryHandler.Instance.Structures.RecastItem.ContainerSize;
            var recastContainerAddress = IntPtr.Add(RecastMap, (int) type * recastContainerSize);

            var container = new ActionContainer {
                Type = type
            };

            var canUseKeyBinds = false;

            var hotbarItemSize = MemoryHandler.Instance.Structures.HotBarItem.ItemSize;
            var recastItemSize = MemoryHandler.Instance.Structures.RecastItem.ItemSize;

            int limit;

            switch (type) {
                case HotBarRecast.Container.CROSS_HOTBAR_1:
                case HotBarRecast.Container.CROSS_HOTBAR_2:
                case HotBarRecast.Container.CROSS_HOTBAR_3:
                case HotBarRecast.Container.CROSS_HOTBAR_4:
                case HotBarRecast.Container.CROSS_HOTBAR_5:
                case HotBarRecast.Container.CROSS_HOTBAR_6:
                case HotBarRecast.Container.CROSS_HOTBAR_7:
                case HotBarRecast.Container.CROSS_HOTBAR_8:
                case HotBarRecast.Container.CROSS_PETBAR:
                    limit = 16;
                    break;
                default:
                    limit = 12;
                    canUseKeyBinds = true;
                    break;
            }

            var hotbarItemsSource = MemoryHandler.Instance.GetByteArray(hotbarContainerAddress, hotbarContainerSize);
            var recastItemsSource = MemoryHandler.Instance.GetByteArray(recastContainerAddress, recastContainerSize);

            for (var i = 0; i < limit; i++) {
                var hotbarSource = new byte[hotbarItemSize];
                var recastSource = new byte[recastItemSize];

                Buffer.BlockCopy(hotbarItemsSource, i * hotbarItemSize, hotbarSource, 0, hotbarItemSize);
                Buffer.BlockCopy(recastItemsSource, i * recastItemSize, recastSource, 0, recastItemSize);

                var name = MemoryHandler.Instance.GetStringFromBytes(hotbarSource, MemoryHandler.Instance.Structures.HotBarItem.Name);
                var slot = i;

                if (string.IsNullOrWhiteSpace(name)) {
                    continue;
                }

                var item = new HotBarRecastItem {
                    Name = name,
                    ID = BitConverter.TryToInt16(hotbarSource, MemoryHandler.Instance.Structures.HotBarItem.ID),
                    KeyBinds = MemoryHandler.Instance.GetStringFromBytes(hotbarSource, MemoryHandler.Instance.Structures.HotBarItem.KeyBinds),
                    Slot = slot
                };

                if (canUseKeyBinds) {
                    if (!string.IsNullOrWhiteSpace(item.KeyBinds)) {
                        item.Name = item.Name.Replace($" {item.KeyBinds}", string.Empty);
                        item.KeyBinds = Regex.Replace(item.KeyBinds, @"[\[\]]", string.Empty);
                        var buttons = item.KeyBinds.Split(
                            new[] {
                                '+'
                            },
                            StringSplitOptions.RemoveEmptyEntries).ToList();
                        if (buttons.Count > 0) {
                            item.ActionKey = buttons.Last();
                        }

                        if (buttons.Count > 1) {
                            for (var x = 0; x < buttons.Count - 1; x++) {
                                item.Modifiers.Add(buttons[x]);
                            }
                        }
                    }
                }

                item.Category = BitConverter.TryToInt32(recastSource, MemoryHandler.Instance.Structures.RecastItem.Category);
                item.Type = BitConverter.TryToInt32(recastSource, MemoryHandler.Instance.Structures.RecastItem.Type);
                item.Icon = BitConverter.TryToInt32(recastSource, MemoryHandler.Instance.Structures.RecastItem.Icon);
                item.CoolDownPercent = recastSource[MemoryHandler.Instance.Structures.RecastItem.CoolDownPercent];
                item.IsAvailable = BitConverter.TryToBoolean(recastSource, MemoryHandler.Instance.Structures.RecastItem.IsAvailable);

                var remainingCost = BitConverter.TryToInt32(recastSource, MemoryHandler.Instance.Structures.RecastItem.RemainingCost);

                item.RemainingCost = remainingCost != -1
                                         ? remainingCost
                                         : 0;
                item.Amount = BitConverter.TryToInt32(recastSource, MemoryHandler.Instance.Structures.RecastItem.Amount);
                item.InRange = BitConverter.TryToBoolean(recastSource, MemoryHandler.Instance.Structures.RecastItem.InRange);
                item.IsProcOrCombo = recastSource[MemoryHandler.Instance.Structures.RecastItem.ActionProc] > 0;

                container.Actions.Add(item);
            }

            return container;
        }
    }
}