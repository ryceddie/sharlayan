﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PartyResult.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   PartyResult.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Models.ReadResults {
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    using Sharlayan.Core;
    using Sharlayan.Delegates;

    public class PartyResult {
        public List<uint> NewParty { get; } = new List<uint>();

        public ConcurrentDictionary<uint, PartyMember> PartyMembers => PartyWorkerDelegate.PartyMembers;

        public Dictionary<uint, uint> RemovedParty { get; } = new Dictionary<uint, uint>();
    }
}