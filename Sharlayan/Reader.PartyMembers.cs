﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Reader.PartyMembers.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   Reader.PartyMembers.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan {
    using System;

    using Sharlayan.Core;
    using Sharlayan.Delegates;
    using Sharlayan.Helpers;
    using Sharlayan.Models.ReadResults;

    using BitConverter = Sharlayan.Helpers.BitConverter;

    public static partial class Reader {
        public static bool CanGetPartyMembers() {
            var canRead = Scanner.Instance.Locations.ContainsKey(Signatures.CharacterMapKey) && Scanner.Instance.Locations.ContainsKey(Signatures.PartyMapKey) && Scanner.Instance.Locations.ContainsKey(Signatures.PartyCountKey);
            if (canRead) {
                // OTHER STUFF?
            }

            return canRead;
        }

        public static PartyResult GetPartyMembers() {
            var result = new PartyResult();

            if (!CanGetPartyMembers() || !MemoryHandler.Instance.IsAttached) {
                return result;
            }

            var PartyInfoMap = (IntPtr) Scanner.Instance.Locations[Signatures.PartyMapKey];
            var PartyCountMap = Scanner.Instance.Locations[Signatures.PartyCountKey];

            try {
                var partyCount = MemoryHandler.Instance.GetByte(PartyCountMap);
                var sourceSize = MemoryHandler.Instance.Structures.PartyMember.SourceSize;

                if (partyCount > 1 && partyCount < 9) {
                    for (uint i = 0; i < partyCount; i++) {
                        var address = PartyInfoMap.ToInt64() + i * (uint) sourceSize;
                        var source = MemoryHandler.Instance.GetByteArray(new IntPtr(address), sourceSize);
                        var ID = BitConverter.TryToUInt32(source, MemoryHandler.Instance.Structures.PartyMember.ID);
                        ActorItem existing = null;
                        if (result.RemovedParty.ContainsKey(ID)) {
                            result.RemovedParty.Remove(ID);
                            if (MonsterWorkerDelegate.ActorItems.ContainsKey(ID)) {
                                existing = MonsterWorkerDelegate.GetActorItem(ID);
                            }

                            if (PCWorkerDelegate.ActorItems.ContainsKey(ID)) {
                                existing = PCWorkerDelegate.GetActorItem(ID);
                            }
                        }
                        else {
                            result.NewParty.Add(ID);
                        }

                        var entry = PartyMemberHelper.ResolvePartyMemberFromBytes(source, existing);
                        if (!entry.IsValid) {
                            continue;
                        }

                        if (existing != null) {
                            continue;
                        }

                        PartyWorkerDelegate.EnsurePartyMember(entry.ID, entry);
                    }
                }
                else if (partyCount == 0 || partyCount == 1) {
                    var entry = PartyMemberHelper.ResolvePartyMemberFromBytes(Array.Empty<byte>(), PCWorkerDelegate.CurrentUser);
                    if (entry.IsValid) {
                        var exists = false;
                        if (result.RemovedParty.ContainsKey(entry.ID)) {
                            result.RemovedParty.Remove(entry.ID);
                            exists = true;
                        }
                        else {
                            result.NewParty.Add(entry.ID);
                        }

                        if (!exists) {
                            PartyWorkerDelegate.EnsurePartyMember(entry.ID, entry);
                        }
                    }
                }
            }
            catch (Exception ex) {
                MemoryHandler.Instance.RaiseException(Logger, ex, true);
            }

            try {
                // REMOVE OLD PARTY MEMBERS FROM LIVE CURRENT DICTIONARY
                foreach (var kvp in result.RemovedParty) {
                    PartyWorkerDelegate.RemovePartyMember(kvp.Key);
                }
            }
            catch (Exception ex) {
                MemoryHandler.Instance.RaiseException(Logger, ex, true);
            }

            return result;
        }
    }
}