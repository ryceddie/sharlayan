﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlHelper.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   XmlHelper.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Helpers {
    using System.Linq;
    using System.Text;

    public static class XmlHelper {
        public static string SanitizeXmlString(string xValue) {
            if (xValue == null) {
                return string.Empty;
            }

            var buffer = new StringBuilder(xValue.Length);
            foreach (var xChar in xValue.Where(xChar => IsLegalXmlChar(xChar))) {
                buffer.Append(xChar);
            }

            return buffer.ToString();
        }

        private static bool IsLegalXmlChar(int xChar) {
            return xChar == 0x9 || xChar == 0xA || xChar == 0xD || xChar >= 0x20 && xChar <= 0xD7FF || xChar >= 0xE000 && xChar <= 0xFFFD || xChar >= 0x10000 && xChar <= 0x10FFFF;
        }
    }
}