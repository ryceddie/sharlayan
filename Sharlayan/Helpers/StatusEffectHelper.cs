﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusEffectHelper.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   StatusEffectHelper.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Helpers {
    using System.Collections.Concurrent;
    using System.Linq;

    using Sharlayan.Models;

    public static class StatusEffectHelper {
        private static StatusItem DefaultStatusInfo = new StatusItem {
            Name = new Localization {
                Chinese = "???",
                English = "???",
                French = "???",
                German = "???",
                Japanese = "???",
                Korean = "???"
            },
            CompanyAction = false
        };

        private static bool Loading;

        private static ConcurrentDictionary<uint, StatusItem> StatusEffects = new ConcurrentDictionary<uint, StatusItem>();

        public static StatusItem GetStatusInfo(uint id) {
            if (Loading) {
                return DefaultStatusInfo;
            }

            lock (StatusEffects) {
                if (StatusEffects.Any()) {
                    return StatusEffects.ContainsKey(id)
                               ? StatusEffects[id]
                               : DefaultStatusInfo;
                }

                Resolve();
                return DefaultStatusInfo;
            }
        }

        internal static void Resolve() {
            if (Loading) {
                return;
            }

            Loading = true;
            APIHelper.GetStatusEffects(StatusEffects);
            Loading = false;
        }
    }
}