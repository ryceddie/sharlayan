﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringHelper.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   StringHelper.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Helpers {
    using System;
    using System.Globalization;
    using System.Text;
    using System.Text.RegularExpressions;

    public static class StringHelper {
        private const RegexOptions DefaultOptions = RegexOptions.Compiled | RegexOptions.ExplicitCapture;

        private static readonly Regex Romans = new Regex(@"(?<roman>\b[IVXLCDM]+\b)", DefaultOptions);

        private static readonly Regex Titles = new Regex(@"(?<num>\d+)(?<designator>\w+)", DefaultOptions | RegexOptions.IgnoreCase);

        private static readonly Regex CleanSpaces = new Regex(@"[ ]+", RegexOptions.Compiled);

        public static string HexToString(string hexValue) {
            var sb = new StringBuilder();
            for (var i = 0; i <= hexValue.Length - 2; i += 2) {
                sb.Append(Convert.ToChar(int.Parse(hexValue.Substring(i, 2), NumberStyles.HexNumber)));
            }

            return sb.ToString();
        }

        public static string TitleCase(string s, bool all = true) {
            if (string.IsNullOrWhiteSpace(s.Trim())) {
                return string.Empty;
            }

            s = TrimAndCleanSpaces(s);
            var result = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(
                all
                    ? s.ToLower()
                    : s);
            var reg = Romans.Match(s);
            if (reg.Success) {
                var replace = Convert.ToString(reg.Groups["roman"].Value);
                var original = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(replace.ToLower());
                result = result.Replace(original, replace.ToUpper());
            }

            var titles = Titles.Matches(result);
            foreach (Match title in titles) {
                var num = Convert.ToString(title.Groups["num"].Value);
                var designator = Convert.ToString(title.Groups["designator"].Value);
                result = result.Replace($"{num}{designator}", $"{num}{designator.ToLower()}");
            }

            return result;
        }

        public static string TrimAndCleanSpaces(string name) {
            return CleanSpaces.Replace(name, " ").Trim();
        }
    }
}