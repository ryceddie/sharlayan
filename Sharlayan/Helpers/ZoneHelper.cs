﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ZoneHelper.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   ZoneHelper.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Helpers {
    using System.Collections.Concurrent;
    using System.Linq;

    using Sharlayan.Models;

    public static class ZoneHelper {
        private static MapItem DefaultZoneInfo = new MapItem {
            Name = new Localization {
                Chinese = "???",
                English = "???",
                French = "???",
                German = "???",
                Japanese = "???",
                Korean = "???"
            },
            Index = 0,
            IsDungeonInstance = false
        };

        private static bool Loading;

        private static ConcurrentDictionary<uint, MapItem> Zones = new ConcurrentDictionary<uint, MapItem>();

        public static MapItem GetZoneInfo(uint id) {
            if (Loading) {
                return DefaultZoneInfo;
            }

            lock (Zones) {
                if (Zones.Any()) {
                    return Zones.ContainsKey(id)
                               ? Zones[id]
                               : DefaultZoneInfo;
                }

                Resolve();
                return DefaultZoneInfo;
            }
        }

        internal static void Resolve() {
            if (Loading) {
                return;
            }

            Loading = true;
            APIHelper.GetZones(Zones);
            Loading = false;
        }
    }
}