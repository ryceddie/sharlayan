// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActionHelper.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   ActionHelper.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Helpers {
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;

    using Sharlayan.Models;

    public static class ActionHelper {
        private static ConcurrentDictionary<uint, ActionItem> _actions;

        private static ActionItem DefaultActionItem = new ActionItem {
            Name = new Localization {
                Chinese = "???",
                English = "???",
                French = "???",
                German = "???",
                Japanese = "???",
                Korean = "???"
            }
        };

        private static bool Loading;

        private static ConcurrentDictionary<uint, ActionItem> Actions {
            get {
                return _actions ?? (_actions = new ConcurrentDictionary<uint, ActionItem>());
            }

            set {
                if (_actions == null) {
                    _actions = new ConcurrentDictionary<uint, ActionItem>();
                }

                _actions = value;
            }
        }

        public static ActionItem ActionInfo(string name) {
            if (Loading) {
                return DefaultActionItem;
            }

            lock (Actions) {
                if (Actions.Any()) {
                    return Actions.FirstOrDefault(kvp => kvp.Value.Name.Matches(name)).Value ?? DefaultActionItem;
                }

                Resolve();
                return DefaultActionItem;
            }
        }

        public static ActionItem ActionInfo(uint id) {
            if (Loading) {
                return DefaultActionItem;
            }

            lock (Actions) {
                if (Actions.Any()) {
                    return Actions.ContainsKey(id)
                               ? Actions[id]
                               : DefaultActionItem;
                }

                Resolve();
                return DefaultActionItem;
            }
        }

        public static List<ActionItem> DamageOverTimeActions() {
            var results = new List<ActionItem>();
            if (Loading) {
                return results;
            }

            lock (Actions) {
                if (Actions.Any()) {
                    results.AddRange(Actions.Where(kvp => kvp.Value.IsDamageOverTime).Select(kvp => kvp.Value));
                    return results;
                }

                Resolve();
                return results;
            }
        }

        public static List<ActionItem> HealingOverTimeActions() {
            var results = new List<ActionItem>();
            if (Loading) {
                return results;
            }

            lock (Actions) {
                if (Actions.Any()) {
                    results.AddRange(Actions.Where(kvp => kvp.Value.IsHealingOverTime).Select(kvp => kvp.Value));
                    return results;
                }

                Resolve();
                return results;
            }
        }

        internal static void Resolve() {
            if (Loading) {
                return;
            }

            Loading = true;
            APIHelper.GetActions(Actions);
            Loading = false;
        }
    }
}