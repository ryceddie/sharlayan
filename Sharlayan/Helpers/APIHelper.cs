﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="APIHelper.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   APIHelper.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sharlayan.Helpers {
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;

    using Newtonsoft.Json;

    using Sharlayan.Models;
    using Sharlayan.Models.Structures;

    using StatusItem = Sharlayan.Models.StatusItem;

    internal static class APIHelper {
        private static WebClient _webClient = new WebClient {
            Encoding = Encoding.UTF8
        };

        public static void GetActions(ConcurrentDictionary<uint, ActionItem> actions) {
            var file = Path.Combine(Directory.GetCurrentDirectory(), "actions.json");
            if (File.Exists(file) && MemoryHandler.Instance.UseLocalCache) {
                EnsureDictionaryValues(actions, file);
            }
            else {
                APIResponseToDictionary(actions, file, "http://xivapp.com/api/actions");
            }
        }

        public static IEnumerable<Signature> GetSignatures(ProcessModel processModel, string patchVersion = "latest") {
            var architecture = processModel.IsWin64
                                   ? "x64"
                                   : "x86";
            var file = Path.Combine(Directory.GetCurrentDirectory(), $"signatures-{architecture}.json");
            if (File.Exists(file) && MemoryHandler.Instance.UseLocalCache) {
                var json = FileResponseToJSON(file);
                return JsonConvert.DeserializeObject<IEnumerable<Signature>>(json, Constants.SerializerSettings);
            }
            else {
                var json = APIResponseToJSON($"http://xivapp.com/api/signatures?patchVersion={patchVersion}&platform={architecture}");
                var resolved = JsonConvert.DeserializeObject<IEnumerable<Signature>>(json, Constants.SerializerSettings);

                File.WriteAllText(file, JsonConvert.SerializeObject(resolved, Formatting.Indented, Constants.SerializerSettings), Encoding.GetEncoding(932));

                return resolved;
            }
        }

        public static void GetStatusEffects(ConcurrentDictionary<uint, StatusItem> statusEffects) {
            var file = Path.Combine(Directory.GetCurrentDirectory(), "statuses.json");
            if (File.Exists(file) && MemoryHandler.Instance.UseLocalCache) {
                EnsureDictionaryValues(statusEffects, file);
            }
            else {
                APIResponseToDictionary(statusEffects, file, "http://xivapp.com/api/statuses");
            }
        }

        public static StructuresContainer GetStructures(ProcessModel processModel, string patchVersion = "latest") {
            var architecture = processModel.IsWin64
                                   ? "x64"
                                   : "x86";
            var file = Path.Combine(Directory.GetCurrentDirectory(), $"structures-{architecture}.json");
            if (File.Exists(file) && MemoryHandler.Instance.UseLocalCache) {
                return EnsureClassValues<StructuresContainer>(file);
            }

            return APIResponseToClass<StructuresContainer>(file, $"http://xivapp.com/api/structures?patchVersion={patchVersion}&platform={architecture}");
        }

        public static void GetZones(ConcurrentDictionary<uint, MapItem> mapInfos) {
            // These ID's link to offset 7 in the old JSON values.
            // eg: "map id = 4" would be 148 in offset 7.
            // This is known as the TerritoryType value
            // - It maps directly to SaintCoins map.csv against TerritoryType ID
            var file = Path.Combine(Directory.GetCurrentDirectory(), "zones.json");
            if (File.Exists(file) && MemoryHandler.Instance.UseLocalCache) {
                EnsureDictionaryValues(mapInfos, file);
            }
            else {
                APIResponseToDictionary(mapInfos, file, "http://xivapp.com/api/zones");
            }
        }

        private static T APIResponseToClass<T>(string file, string uri) {
            var json = APIResponseToJSON(uri);
            var resolved = JsonConvert.DeserializeObject<T>(json, Constants.SerializerSettings);

            File.WriteAllText(file, JsonConvert.SerializeObject(resolved, Formatting.Indented, Constants.SerializerSettings), Encoding.UTF8);

            return resolved;
        }

        private static void APIResponseToDictionary<T>(ConcurrentDictionary<uint, T> dictionary, string file, string uri) {
            var json = APIResponseToJSON(uri);
            var resolved = JsonConvert.DeserializeObject<ConcurrentDictionary<uint, T>>(json, Constants.SerializerSettings);

            foreach (var kvp in resolved) {
                dictionary.AddOrUpdate(kvp.Key, kvp.Value, (k, v) => kvp.Value);
            }

            File.WriteAllText(file, JsonConvert.SerializeObject(dictionary, Formatting.Indented, Constants.SerializerSettings), Encoding.UTF8);
        }

        private static string APIResponseToJSON(string uri) {
            return _webClient.DownloadString(uri);
        }

        private static T EnsureClassValues<T>(string file) {
            var json = FileResponseToJSON(file);
            return JsonConvert.DeserializeObject<T>(json, Constants.SerializerSettings);
        }

        private static void EnsureDictionaryValues<T>(ConcurrentDictionary<uint, T> dictionary, string file) {
            var json = FileResponseToJSON(file);
            var resolved = JsonConvert.DeserializeObject<ConcurrentDictionary<uint, T>>(json, Constants.SerializerSettings);

            foreach (var kvp in resolved) {
                dictionary.AddOrUpdate(kvp.Key, kvp.Value, (k, v) => kvp.Value);
            }
        }

        private static string FileResponseToJSON(string file) {
            using (var streamReader = new StreamReader(file)) {
                return streamReader.ReadToEnd();
            }
        }
    }
}