﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="SyndicatedLife">
//   Copyright(c) 2017 Ryan Wilson &amp;lt;syndicated.life@gmail.com&amp;gt; (http://syndicated.life/)
//   Licensed under the MIT license. See LICENSE.md in the solution root for full license information.
// </copyright>
// <summary>
//   Program.cs Implementation
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bootstrapper {
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Xml;
    using System.Xml.Linq;

    using NLog;
    using NLog.Config;

    using Sharlayan;
    using Sharlayan.Events;
    using Sharlayan.Helpers;
    using Sharlayan.Models;

    class Program {
        static void Main(string[] args) {
            var stringReader = new StringReader(XElement.Load("./Bootstrapper.exe.nlog").ToString());

            using (var xmlReader = XmlReader.Create(stringReader)) {
                LogManager.Configuration = new XmlLoggingConfiguration(xmlReader, null);
            }

            ActionHelper.ActionInfo(2);
            StatusEffectHelper.GetStatusInfo(2);
            ZoneHelper.GetZoneInfo(138);

            var process = Process.GetProcessesByName("ffxiv_dx11").FirstOrDefault();

            MemoryHandler.Instance.SetProcess(
                new ProcessModel {
                    IsWin64 = true,
                    Process = process
                });

            while (Scanner.Instance.IsScanning) {
                Thread.Sleep(1000);
                Console.WriteLine("Scanning...");
            }

            MemoryHandler.Instance.SignaturesFoundEvent += delegate(object sender, SignaturesFoundEvent e) {
                foreach (var kvp in e.Signatures) {
                    Console.WriteLine($"{kvp.Key} => {kvp.Value.GetAddress():X}");
                }

                Console.WriteLine("To exit this application press \"Enter\".");
                Console.ReadLine();
            };
        }
    }
}